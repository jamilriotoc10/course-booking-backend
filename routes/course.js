const Course = require("../models/course")
const CourseController = require("../controllers/course")
const express = require("express")
const router = express.Router()
const auth = require("../auth")


router.get('/', (req,res) => {


	CourseController.getAllActiveCourse().then(resultFromGetAllCourse => res.send(resultFromGetAllCourse))
}) //all users, admin, or regular 

//ACTIVITY 11 create a route which will get All courses.
//only a user with proper token can use this route.
//our client should receive all of the courses
router.get('/all', auth.verify, (req, res) => {

	CourseController.getAllCourse().then(resultFromGetAllCourse => res.send(resultFromGetAllCourse))
})



//  ACTIVITY 7
router.get('/:id', (req,res) => {

	//console.log(auth)

	CourseController.getCourseDetail(req.params.id).then(resultFromGetCourseDetail => res.send(resultFromGetCourseDetail))
})


//CHANGE COURSE DETAILS





/*router.put('/activate/:id', (req,res)=>{

	Course.findById(req.params.id, (findErr, foundCourse) => {

		foundCourse.isActive = true

		foundCourse.save((errUpdate, updatedCourse) => {

			

			if(errUpdate) return console.log(errUpdate)

			return res.status(200).json({
				message: `Course ${updatedCourse.name} is available.`
			})
		})
	})

})

router.put('/deactivate/:id', (req,res)=>{

	Course.findById(req.params.id, (findErr, foundCourse) => {

		foundCourse.isActive = false

		foundCourse.save((errUpdate, updatedCourse) => {

			

			if(errUpdate) return console.log(errUpdate)

			return res.status(200).json({
				message: `Course ${updatedCourse.name} is unavailable.`
			})
		})
	})

}) 
*/

/*
	Create a put method route which will update a course from active to inactive.
	Create a put method route which will update a course from inactive to active.
	
*/
//create a new post method route to create a course
router.post("/", auth.verify, (req, res) => { //add a middleware, from auth, so that not everybody can have access to create or add a new course.

	CourseController.registerNewCourse(req.body).then(resultFromNewCourse => res.send(resultFromNewCourse))

})


//UPDATE A COURSE
router.put('/', auth.verify,(req,res)=>{

	

	CourseController.update(req.body).then(resultFromUpdate => res.send(resultFromUpdate))
})




/*console.log(req.params)
	//can contain an id; values which can be added to the url.
	//:id -> defined route parameter becomes the key/property in your request params.


	console.log(req.headers)
	//headers are additional information sent by the request. We typically or we should send sensitive information such as token through our hedears, because our headers are typically not manipulated or changed.

	console.log(req.body)
	//req.body contains the bulk of your request, the input or data that you are going to use to process. */

	//modules - modules are libraries of functions. Some modules have pre-defined functions especially if they are part of package by NPM. We can also define our own functions in a custom module 







// ACTIVITY 9 update a course name, description, and price
router.put("/",  auth.verify, (req,res) => {

	//console.log(req.params)
//console.log(req.body)

	UserController.userUpdate(req.body,req.params).then(resultUserUpdate => res.send(resultUserUpdate))
})


// ACTIVITY 8 
router.put('/activate/:id', auth.verify,(req,res) => {

console.log(req.params)
	CourseController.activate(req.params).then(resultFromActivate => res.send(resultFromActivate))

})

router.put('/deactivate/:id', auth.verify,(req,res) => {

	console.log(req.params)
	CourseController.deactivate(req.params).then(resultFromDeactivate => res.send(resultFromDeactivate))


})






module.exports = router


