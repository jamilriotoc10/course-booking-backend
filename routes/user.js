const User = require("../models/user")
const UserController = require("../controllers/user")
const express = require("express")
const router = express.Router()
const auth = require("../auth")




router.get("/", (req, res) => {

	UserController.getAllUsers().then(resultFromGetAllUsers => res.send(resultFromGetAllUsers))

})
	



router.post("/", (req, res) => {
	
	UserController.register(req.body).then(resultFromRegister => res.send(resultFromRegister))

})

//route to check if there is an email that exists or not:
router.post('/email-exists', (req,res) => {

	UserController.emailExists(req.body).then(resultFromEmailExists => res.send(resultFromEmailExists))
	
})


router.post('/login', (req,res) => {

	UserController.login(req.body).then(resultFromLogin => res.send(resultFromLogin))

})








//ENROLL

router.post('/enroll', auth.verify, (req,res) => {

	//define an object to be sent into our enrollment function. This object will contain our user's id and the course id he is trying to enroll in. This route should only have the courseId as part of the body. We are going to get our user's id from the token, instead. To do this, we have to decode the token being passed

	//console.log(req.body)

	let userData = auth.decode(req.headers.authorization) //decode results to the data payload in our token. 

	//console.log(userData.id)

	let params ={

		userId: userData.id,
		courseId: req.body.courseId 


	}

	UserController.enroll(params).then(resultFromEnroll => res.send(resultFromEnroll))

})







/*http://localhost:4000*/

//get the details of a user.
//auth.verify ensures that a user is properly logged in before proceeding to the next part of the code. 
//auth.verify will act as a middleware, wherein it acts like a gate and only those who has legit permission can enter
router.get('/details', auth.verify, (req,res) => {
//Create a controller function which uses the details endpoint. This controler function is able to get a single user. As you notice, we no longer put the id in the paramerter. Include the user id in the request body instead. Return the details of our user except for the most sensitive information in his document. 
	

	const user = auth.decode(req.headers.authorization) 

	console.log(user)

	UserController.get({userId: user.id}).then(user => res.send(user))
})


//ACTIVITY 10
router.put("/", auth.verify, (req,res) => {

	//console.log(req.params)
  console.log(req.body)

	UserController.detailUpdate(req.body).then(resultDetailUpdate => res.send(resultDetailUpdate))
})

//Integration Routes
//Adding an async keyword to a function, creates an asynchronous function.
//await keyword can only be used in an async function.
//JS is by default, sychronous.
//statement1 -> statement2 -> statement3
//When we want to wait for a function to resolve itself, we can make it async.

router.post('/verify-google-id-token', async (req,res)=>{

	res.send(await UserController.verifyGoogleTokenId(req.body.tokenId,req.body.accessToken))

})

module.exports = router


