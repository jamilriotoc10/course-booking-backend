const express = require('express'); //import the Express package from node_modules
const app = express()//store Express' express function in a variable
const mongoose = require('mongoose') //import Mongoose into our project
const cors = require('cors') //import cors into our project
/*const nodemailer = require('nodemailer')*/

app.use(express.json()) //Middleware - only allows requests if they are in JSON format
app.use(express.urlencoded({ extended: true})) // allows POST requests to include nested objects
app.use(cors())

mongoose.connection.once('open',() => console.log('Now connected to MongoDB Atlas.'))
mongoose.connect('mongodb+srv://jamilriotoc:riotoc123@cluster0.kj492.mongodb.net/restbooking?retryWrites=true&w=majority', {
	useNewUrlParser: true,
	useUnifiedTopology: true
})

/*const transporter = nodemailer.createTransport({

	host: 'smtp.gmail.com',
	//port 465 is a secured port with SSL for gmail
	port: 465,
	secure: true,
	auth: {

		user: 'banalnapato10@gmail.com',
		pass: 'Testemail10!'

	},
	tls: {

		rejectUnauthorized: false
	}
})

transporter.sendMail({

	from: 'test@example.com',
	//ethereal account to send to
	to: 'banalnapato10@gmail.com',
	subject: 'Zuitt Booking System',
	//If email doesn't accept HTML format email, send regular text.
	text: 'Good Day!',
	html: '<h3>Hello there!! I would like to invite you to register in our booking system. There are tons of programming stuffs that you can learn from Zuitt Programming Bootcamp. I, for one, really did enjoy learning different programming courses like NextJS and ReactJS. We would like you to be a part of the programming world! Thank you and have a nice day!!!</h3>'
})
*/

const userRoutes = require('./routes/user')// import our routes so that we may be able to use the routes to where we can make our requests to this server and this server to make its responses
const courseRoutes = require('./routes/course')


app.use('/api/users', userRoutes) // our middleware, which means it is where we put our endpoints through.
//http://localhost:4000/api/users/<endpoint>/<route>
app.use('/api/courses', courseRoutes)
//http:localhost:4000/api/courses/<endpoint>or<route>

app.listen(process.env.PORT || 4000, () => {
	console.log(`API is now online on port ${ process.env.PORT || 4000}`)
}) //for starting the server

