/*Controllers - are mostly functions which are then exported to be used in another file and that is in our routes*/

const User = require("../models/user")
const Course = require("../models/course")
//require : bcrypt
const bcrypt = require("bcrypt")
//if the module is from npm, we only need to put the name of tha package, on the other hand, if the file/module is custom made, we have to put the path of the file

const auth = require("../auth")

const {OAuth2Client} = require('google-auth-library') //this will allow us to use Google Login Client and verify the token
const clientId = '52292830160-kocgbufigg8tnf9ns5qa4j2aotrerh4j.apps.googleusercontent.com'
//Reminder: The Client ID here in the backend MUST match the client ID used in the frontend. 
//It may not cause immediate erroe during local development but it flares up an error when hosted. 
const { google } = require("googleapis");
const OAuth2 = google.auth.OAuth2;
const nodemailer = require('nodemailer');
require('dotenv').config();
//Functions
//Check if an email already exists

module.exports.emailExists = (parameterFromRoute) => {

	//Steps 
	/*
		1. Use mongoose's find() with the search criteria for email.
		2. .then() - runs a codeblock once a method wherein it was attached has finished its process and returns a response or a result.
		3. Using the result from find, we're going to check if the length of the array that is going to be retunred by the query has a length of 0 or not.
		5. use a ternary operator which will return either true of false. 

	*/

	return User.find({ email: parameterFromRoute.email }).then(resultFromFind => {

		return resultFromFind.length > 0 ? true : false
	})


}

module.exports.register = (params) => {

	let newUser = new User({

		firstName: params.firstName,
		lastName: params.lastName,
		email: params.email,
		mobileNo: params.mobileNo,
		password: bcrypt.hashSync(params.password, 10)
		//hashSync() hashes/encrypts our string provided to the number of salt. In our case: 10

	})

	return newUser.save().then((user, err) => {

		return (err) ? false : true

	})

}

module.exports.getAllUsers = () => {

	return User.find({}).then(resultFromFind => resultFromFind)


}

//login
module.exports.login = (params) => {
			
			//mongoose query - findOne() - it is much efficient to current use the find a single document.
			//If you're in doubt about the value of a variable, console.log()

	return User.findOne({email: params.email}).then(resultFromFindOne => {

		console.log(resultFromFindOne)

			//there is no user of the same email, run this code:
		if(resultFromFindOne == null){

			return false

		} 

		const isPasswordMatched = bcrypt.compareSync(params.password, resultFromFindOne.password)

		//compareSync() is used to compare our users' password. The one that they input against the hashed password saved in our database. 

		console.log(isPasswordMatched)

		if(isPasswordMatched){

			return { accessToken: auth.createAccessToken(resultFromFindOne)}

		} else {

			return false
		}

	})

}

module.exports.get = (params) => {

		console.log(params)


	return 	User.findById(params.userId).then(resultFromFindById => {

		//console.log(resultFromFindById)
		resultFromFindById.password = undefined

		return resultFromFindById

	})

} //ACTIVITY 10

module.exports.detailUpdate = (body) => {

	console.log(body)
	//console.log(courseId)
	//console.log(req)

	let updateDetails = {

		firstName: body.firstName,
		lastName: body.lastName,
		mobileNo: body.mobileNo

	}

	return User.findByIdAndUpdate(body.id, updateDetails).then((user, err) => {


		return (err) ? false : true
	})


}

module.exports.enroll =(params) => {

//console.log(params)

/*
	1. get the user document.
	2. push the courseId into the user's enrollment array.
	3. save the updated user.
	4. get the course. 
	5. push the user's id to the enrollee array of our course.
	6. save the updated course. 

*/
	//find the user by its id
	return User.findById(params.userId).then(resultFromFindByIdUser => {

		resultFromFindByIdUser.enrollments.push({ courseId: params.courseId })
		//we now have enrolled our student into the course by adding the course into his enrollments array.

		return resultFromFindByIdUser.save().then((resultFromSaveUser, err) =>	{

			//Find the course by its id
			return Course.findById(params.courseId).then(resultFromFindByIdCourse => {

				resultFromFindByIdCourse.enrollees.push({userId: params.userId})
				//this course has now the student enrolled.

				return resultFromFindByIdCourse.save().then((resultFromSaveCourse, err) => {

					return (err) ? false : true
				})

			}) 
		})

	})

}

//Google Login


module.exports.verifyGoogleTokenId = async (tokenId,accessToken) => { 

	console.log(tokenId)
	console.log(accessToken)

	//creates a new OAuth2Client with our clientId for identification
	const client = new OAuth2Client(clientId)
	const data = await client.verifyIdToken({idToken: tokenId, audience: clientId})
	//audience and idToken are required to check your users' google login token and your OAuth2Client's clientsId side by side to check if it's coming from a recongnizable source. 

	//let's check if the data responded by verifyIdToken is correct.
	//console.log(data)

	if(data.payload.email_verified === true){

		//check the db if the email found in the data.payload sent by google is already registered in our db:
		const user = await User.findOne({email: data.payload.email}).exec()
		//.exec() in mongoose, works almost like then() that it allows the execution of the following statements.
		//if the user variable logs null, then the user is not registered in OUR db yet.
		//if the user variable logs details, then the user is already registered in our DB.
		//console.log(user)
		//check if user is null or not:

		if(user !== null){

			console.log('A user with the same email has been registered.')
			if(user.loginType === "google"){

				return {accessToken: auth.createAccessToken(user)}
			
			}else{

				return {error: 'login-type-error'}
			}

		} else {
			//register the first-time our google login user, logs into our app.

			//check in the console the incoming payload of the user's data from google.
			//console.log(data.payload)

			const newUser = new User({

				firstName: data.payload.given_name,
				lastName: data.payload.family_name,
				email: data.payload.email,
				loginType: "google"

			})

			//saves our new user into our db
			return newUser.save().then((user, err) => {

				//we're going to send an email to our google user, when he/she logs in and registers into our app for the first time.
				//mailOptions: the content of the email that we are going to send to our user. This includes the message, the email address of the user, the address of the sender.

				const mailOptions = {
					//the email that you're sending from
					from: 'jamil.riotoc@gmail.com',
					//the email of the new user:
					to: user.email,
					subject: 'Thank you for registering to Zuitt Booking.',
					text: `You registered to Zuitt Booking on ${new Date().toLocaleString()}`,
					html: `You registered to Zuitt Booking on ${new Date().toLocaleString()}`

				}

				//Nodemailer Configuration: gmail with OAuth
				const transporter = nodemailer.createTransport({

					host: 'smtp.gmail.com',
					port: 465,
					secure: true,
					auth: {

						type: 'OAuth2',
						user: process.env.MAILING_SERVICE_EMAIL,
						clientId: process.env.MAILING_SERVICE_CLIENT_ID,
						clientSecret: process.env.MAILING_SERVICE_CLIENT_SECRET,
						refreshToken: process.env.MAILING_SERVICE_REFRESH_TOKEN,
						accessToken: accessToken
					}
				})

				//Create a function to send our email:
				//Uses the transporter as a parameter to send our email with the proper authentications:

				function sendMail(transporter){

					transporter.sendMail(mailOptions, function(err,result){

						if (err){

							console.log(err)

						} else if(result){

							console.log(result)
							transport.close()

						}
					})
				
				}

				sendMail(transporter)

				//creates a token from our auth module
				return {accessToken: auth.createAccessToken(user)}

			})
		}

	} else {//google auth error if the google tokenId is compromised or there had been an error while checking the tokenId

		return {error: "google-auth-error"}

	}

}
