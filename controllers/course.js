const Course = require("../models/course")


module.exports.getAllActiveCourse = () => {
 
	return Course.find({ isActive: true }).then(resultFromFind => resultFromFind)

}

module.exports.registerNewCourse = (params) => {

	let newCourse = new Course({

		name: params.name,
		description: params.description,
		price: params.price
		

	})

	return newCourse.save().then((course, err) => {

		return (err) ? false : true
	})
}


// ACTIVITY 7 

module.exports.getCourseDetail = (params) => {

		
	return Course.findById(params).then(resultFromFindById => resultFromFindById)

} 


module.exports.deactivate = (params) => {

	//console.log(params)

	/* 
		Last time, wehad to use findById and the result of the query was updated manually and to save the changes, we used .save()

		This time, we are going to use .findByIdAndUpdate()
		
		.findByIdAndUpdate() - is a mongoose query, wherein mongoose finds the document by its id, and applies the updates.

		Mode.findByIdAndUpdate(<document id>, <changes>)
	*/
	console.log(params)
	let updateActive = {

		isActive: false
	}

	return Course.findByIdAndUpdate(params.id, updateActive).then((course, err) =>{

		return (err) ? false : true
		//check if the updating had an error, if an error was made, false will be returned to the client, otherwise, it will be true.
	})

}
//activity 8
module.exports.activate = (params) => {

	let updateActive = {

		isActive: true
	}

	return Course.findByIdAndUpdate(params.id, updateActive).then((course, err) =>{

		return (err) ? false : true
	})

}

//ACTIVITY 11 
module.exports.getAllCourse = () => {
 
	return Course.find().then(resultFromFindAllCourse => resultFromFindAllCourse)

}




//ACTIVITY 9
module.exports.update = (details) => {

	//console.log(body)
	//console.log(courseId)
	//console.log(req)

	let updateDetails = {

		name: body.name,
		description: body.description,
		price: body.price

	}

	return Course.findByIdAndUpdate(details.courseId.id, updateDetails).then((course, err) => {


		return (err) ? false : true
	})


}


