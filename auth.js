const jwt = require("jsonwebtoken")
const secret = "CourseBookingAPI" // secret can be any phrase

/*
	JWT is like a gift wrapping service but with secrets.
	Only the person with the secret can open the gift.
	And if the wrapper has been tampered with, JWT also recognizes this and disregards the gift.

	Meaning that if the token that we provide to our user has been tampered with, or does not have the correct secret, they should not be able to access certain functions in our app.

*/


//Functionality
//Create Access Token -> pack our gift and sign it with the secret.

module.exports.createAccessToken = (user) => {

	//user paramerter will be provided from our login.

	const data = {

		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin

	}

	return jwt.sign(data, secret, {})

}

//verify the toke -> to check if the token is legit.

module.exports.verify = (req, res, next) => {

//we will put JWT in the headers of our request and because this function will send a response to the client. 

let token = req.headers.authorization
//This is in postman as: Authorization -> Bearer Token -> Token

//console.log(token)
	// this will check if a token exists or passed
	if(typeof token !== "undefined"){

		token = token.slice(7, token.length)
		//<string>/slice - cuts the string starting from the value up to the specified value
		//start at the 7th and end at the last character
		//The first 7 characters are not relevant/related to the actual data we need.
		/*console.log(token)*/

		return jwt.verify(token, secret, (err, data)=>{
			//if there is an error in the verification of the token, the verification fails.
			return (err) ? res.send({auth: "failed"}) : next ()
			//next() is a function that allows to proceed to the next request.

		})

	} else {

		//if a token is empty
		return res.send({auth: "failed"})
	}

}

//decode the token -> open the gift and unraveling the contents
module.exports.decode = (token) =>{ //we're going to pass the token from the route

	if(typeof token !== "undefined"){

		token = token.slice(7, token.length)

		return jwt.verify(token, secret, (err,data)=>{

			return (err) ? null : jwt.decode(token, {complete:true}).payload
			//jwt.decode - decodes our token and gets the payload.
			//payload in our case is the passed data from our createAccessToken

		})

	} else {

		return null

	}

}