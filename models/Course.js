const mongoose = require('mongoose');

const courseSchema = new mongoose.Schema({

	name: {
		type: String,
		required: [true, "Super Course"]
	},

	description: {
		type: String, 
		required: [true, "This is the Super Course"]
	},

	price: {
		type: Number,
		required: [true, 1000.00]
	},

	isActive: {
		type: Boolean,
		default: true
	},

	createdOn: {
		type: Date,
		default: new Date()
	},

	enrollees: [
		{
			userId: {
				type: String,
				required: [true, "Enrollee #1"]
			},

			enrolledOn: {
				type: Date,
				default: new Date()//logs the current date when user was enrolled.
			}



		}

	]





})

module.exports = mongoose.model('Course', courseSchema)
//allows us to use the model as a module and can be required on other files that need it.
