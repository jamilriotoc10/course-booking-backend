const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
	firstName: {
		type: String,
		required: [true, "First name is required."]
	},
	lastName: {
		type: String,
		required: [true, "Last name is required."]
	},
	email: {
		type: String,
		required: [true, "Email is required."]
	},
	password: {
		type: String,
		
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	mobileNo: {
		type: String,
		
	},
	//Google Login: added logintype in model:
	//This will determine whether the new users registered regularly or via google login.
	loginType: {

		type: String,
		required: [true, 'Login Type is required.']


	},
	enrollments: [
		{
			courseId: {
				type: String,
				required: [true, "Course ID is required."]
			},
			enrolledOn: {
				type: Date,
				default: new Date() //use current typestamp
			},
			status: {
				type: String,
				default: "Enrolled" //Alternative values : "Cancelled" or "Completed"
			}
		}
	]
})

module.exports = mongoose.model('user', userSchema)